const sessions = []

// sessions.push({
//   token: 'm72jix1rwd2gh3y7xmtww8s6d',
//   id: 1
// })
function guardarSession(obj) {
  
  sessions.push(obj)
  
}

function isActiveSession(token) {
  return true
}

function getIdByToken(token) {
  const userid = sessions.find(s => s.token == token)
  if (userid == null || userid == undefined) {
    throw { sendStatus: 401, message: "ERROR SESION INVALIDA" }
  }
  // console.log('user id:' + userid)
  return userid.id
}



module.exports = {
  guardarSession,
  isActiveSession,
  getIdByToken
}