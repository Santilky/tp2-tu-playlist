
const ranks = []

const rankGen = []
rankGen.push({ id: 1, userId: 1, genId: 2, score: 8 })
rankGen.push({ id: 2, userId: 1, genId: 7, score: 5 })
rankGen.push({ id: 3, userId: 1, genId: 5, score: 2 })
const songrepo = require('./songrepo')
const userrepo = require('./userrepo')
const rankGenModel = require('../schemas/rankgen')
const rankModel = require('../schemas/rank')
const db = require('../db')

const fs = require('fs')

async function findRankGen(robj) {
    return await db.db.valgen.findOne({
        where: {
            userId: robj.userId,
            genId: robj.genId
        }
    })
}

async function findRankSong(robj) {
    return await db.db.valsongs.findOne({
        where: {
            songId: robj.songId,
            userId: robj.userId
        }
    })
}

async function findRankGensById (userid) {
    return await db.db.valgen.findAll({
        where: {
            userId: userid
        }
    })
}
async function findRankSongsById(userid) {
    return await db.db.valsongs.findAll({
        where: {
            userId: userid
        }
    })
}
async function addValidatedRankGen(obj) {
    return await db.db.valgen.create(obj)
} 
async function addValidatedRankSong(obj) {
    return await db.db.valsongs.create(obj)
} 
async function updateValGen(obj) {
    return await db.db.valgen.update({
        genId: obj.genId,
        userId: obj.userId,
        score: obj.score
    },{
    where: {
        genId: obj.genId,
        userId: obj.userId
    }})
}
async function updateValSong(obj) {
    return await db.db.valsongs.update({
        songId: obj.songId,
        userId: obj.userId,
        score: obj.score
    },{
    where: {
        songId: obj.songId,
        userId: obj.userId
    }})
}
// async function valorar(robj, handler) {
    
    
//         const rankexistente = ranks.find(object => object.userId == robj.userId && object.songId == robj.songId)
//         if (rankexistente) {
//              rankexistente.score = robj.score
//              console.log("Valoracion de score Update!")
//             return rankexistente
//         } else {
//             robj.id = rid++
//             ranks.push(robj)
//             console.log(robj)
//             return robj
//         }
     
// }


// async function valorarGenero(rgobj) {
//    // console.log("a ver antes de llamar a validate")
//     const obj = await rankGenModel.validate(rgobj)
//    // console.log(obj)
//    const valgenenc = rankGen.find(object => object.userId == rgobj.userId && object.genId == rgobj.genId)
//    if (valgenenc) {
//       // throw { sendStatus: 404, message: "La valoracion ya existe" }
//       //si ya existe la actualiza
//       valgenenc.score = rgobj
//       console.log("Valoracion de genero actualizada!")
//       return valgenenc
//    }
//    if (!songrepo.getGeneros().find(g => g.id == rgobj.genId)) {
//     throw { sendStatus: 404, message: "ERROR INEXISTENCIA" }
//    }
//    if (!userrepo.getUser(rgobj.userId)) {
//     throw { sendStatus: 404, message: "ERROR INEXISTENCIA" }
//    }
//     rgobj.id = rgid++
//     rankGen.push(rgobj)
//    // console.log(rgobj)
//     return rgobj

// }

async function getRankGenerosUserId(id) {
    return await db.db.valgen.findAll({
        where: {
            userId: id
        }
    })
}
async function getRankCancionesUserGen(userId, genId) {
    const userValSongs = await db.db.valsongs.findall({
        where: {
            userId: userId,
        }
    })
    const arr = []
    for (const valsong of userValSongs) {
        if (await db.db.songs.findOne({ 
            where: {
                songId: valsong.songId,
                genre: genId
            }
        })) {
            
            arr.push(valsong)
        }
    }
    return arr
}
async function getRankCancionesUser(userId) {
    return db.db.valsongs.findAll({
        where:  {
            userId: userId
        }
    })
}
module.exports = {
    getRankGenerosUserId,
    getRankCancionesUserGen,
 //   valorar,
 //   valorarGenero,
    getRankCancionesUser,
    findRankGen,
    addValidatedRankGen,
    findRankSong,
    addValidatedRankSong,
    findRankGensById,
    findRankSongsById,
    updateValSong,
    updateValGen
}