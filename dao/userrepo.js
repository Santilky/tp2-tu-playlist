const users = []

users.push({
    username: 'pepe',
    password: '1234ww',
    repeat_password: '1234ww',
    email: 'estaseai@lete.com',
    id: 1

  })
  users.push({
    username: 'pepito',
    password: '123456',
    repeat_password: '1234ww',
    email: 'esti@lete.com',
    id: 2

  })

const db = require('../db')
const userModel = require('../schemas/user')

let uid = 3

// function login(user, pass) {
//     if (users.find(obj => obj.username == user && obj.password == pass)) {
//         return true
//     } else {
//         return false
//     }
// }
async function validarUsuarioRepetido(username, email) {
    let found = await db.db.users.findOne({
        where: {
            username: username
            }
        })
    if (found) {
        throw { sendStatus: 401, message: "el id o mail ya están en uso" }
    }
    found = await db.db.users.findOne({
        where: {
            email: email
        }
    })
    if (found) {
        throw { sendStatus: 401, message: "el id o mail ya están en uso" }
    }
}
async function agregarUsuarioValidado(userObject) {
    const creado = {
        username: userObject.username,
        password: userObject.password,
        email: userObject.email,
      }
    const newUser = await db.db.users.create(creado)
    //users.push(creado)
 //  console.log(userObject)
    return {
        username: newUser.username,
        email: newUser.email,
        id: newUser.id
    }
}




async function buscarUserMailOPass(loginObject) {
    const user = await db.db.users.findOne({
        where: {
            username: loginObject.username
        }
    })
  //  console.log(users)
  //  console.log(user)
    if (user) {
   //     console.log("yeah!")
        if (user.password != loginObject.password) {
            throw {sendStatus: 401, message: "Credenciales invalidas"}
        }
        return user.id
    } else {
    //    console.log("ohh!")
        throw {sendStatus: 401, message: "Credenciales invalidas"}
        return false
    }
}


async function getUser(id) {
//    console.log("id es " + id)
    const user = await db.db.users.findOne({
        where: {
            id: id
        }
    })
    if (user == null || user == undefined) {
        throw { sendStatus: 404, message: "ERROR INEXISTENCIA" }
    }
    
    return {
        username: user.username,
        id: user.id,
    }
    
}


module.exports = {
    agregarUsuarioValidado,
    buscarUserMailOPass,
    validarUsuarioRepetido,
    getUser
}
