const generos = []
const songdir = require('./songadrdao')
const canciones = []
const fs = require('fs')
let cid = 1
let gid = 1
let maxgen = 0
const db = require('../db')
async function getSongs(filedir) {
    try {
        console.log('Cargando Musica a la DB')
        const archivos = JSON.parse(fs.readFileSync(filedir))
        for (const cancion of archivos) {
            try {
                
               if (cancion.genre != undefined) {
                const genre = await separarStringGenero(cancion.genre)
                //console.log(genre[0])
                const newCancion = {
                //    address: cancion.address,
                    artist: cancion.artist,
                    album: cancion.album,
                    title: cancion.title,
                    genre : genre[0]
                }
                const repe = await db.db.songs.findOne({
                    where: {
                        artist: cancion.artist,
                        album: cancion.album,
                        title: cancion.title
                    }
                })
            //    console.log('===============================')
            //    console.log(repe)
                if (repe == null) {
                    const nwCancion = await db.db.songs.create(newCancion)
                    await db.db.songsadr.create({
                        address: cancion.address,
                        songId: nwCancion.id
                    })
                }
          //      console.log(cancion.title + 'se ha agregado con exito')
               }
               
            //    canciones.push(newCancion)
                //songdir.addSongDir(cancion.address, cancion.id)
              
            } catch (error) {
                console.log(error)
            }
        }
    //    console.log('Musica Cargada a la dB con exito')
        maxgen = generos.length
    } catch (error) {
       // console.log(error)
    }
}
function getMaxGen() {
    return maxgen
}
async function separarStringGenero(nomString) {
    if (nomString == undefined) {
        throw error = "DB: Genero is undefined"
    } else {
        // console.log(nomString)
        const generos = nomString.split('/')
       // console.log(generos)
        //  console.log(generos.length)
        //Saca espacios, pone todo en mayusuculas y mueve a base de datos, crea un nuevo genero si no lo encuentra
        const newGeneros = []
       // let i = 0
       for (const element of generos) {
        let newName = ""
        for (const chr of element.toUpperCase()) {
            if (chr != ' ') {
                newName = newName + chr
            }
        }
        const ng = await newGenre(newName)
        newGeneros.push( ng) 
       }
       //console.log(newGeneros)
        return newGeneros
    }
}
async function newGenre(str) {
  //  let obj = generos.find(object => object.name == str)
    let obj = await db.db.generos.findOne({
        where: {
            name: str
        }
    })
    if (obj) {
        return obj.id
    } else {
        const newGenero = await db.db.generos.create({
            name: str
        })
        //generos.push(newGenero)
    //    console.log('DB: Se ha creado un nuevo genero: ' + str)
        return newGenero.id
    }
}
async function getGeneros() {
    return await db.db.generos.findAll()
}

async function getCancionesPorGeneroID(genId) {
    // let select = canciones.filter(function (object) {
    //     if (object.genre != undefined && object.genre.find(obj => obj == genId) > 0)
    //     {
    //         return true
    //     } else {
    //         return false
    //     }
    // })
  //  console.log("rompo")
 // console.log(genId)
    let select = await db.db.songs.findAll({
        where: {
            genre: genId
        }
    })
   // console.log(select)
    return select
}


async function getSong(id) {
  //  const obj = canciones.find(obj => obj.id == id)
    const obj = await db.db.songs.findOne({
        where: {
            id: id
        }
    })
    return obj
}

async function getCanciones() {
    return await db.db.songs.findAll()
}
module.exports = {
    getSongs,
    getSong,
    getCanciones,
    getCancionesPorGeneroID,
    getGeneros,
    getMaxGen
}