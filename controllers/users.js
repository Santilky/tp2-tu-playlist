const express = require('express')
const router = express.Router()
const userService = require('../servicios/users')
const sessionsserv = require('../servicios/sessions')

router.post('/', async function (req, res) {
    const body = req.body
    
    try {
     //   console.log("hasta aca llegox3")
      const obj = await userService.registerUsuario(body)
   //   console.log(obj)
 //  console.log("hasta aca llegox2")
      res.send(obj)  
    } catch (error) {
     //  console.log(error.message)
       res.sendStatus(error.sendStatus)
    } 
})

router.get('/:token',async function (req, res) {
   const token = req.params.token
   
   try {
       const obj = {user:
          await userService.getUserById(await sessionsserv.estaLogeado(token))
       }
 //      console.log('tengo un user!')
       res.send(obj)
   } catch (error) {
       res.sendStatus(error.sendStatus)
   }
})
module.exports = router