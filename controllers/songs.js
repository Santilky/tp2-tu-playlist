const express = require('express')
const router = express.Router()
//const db = require('../dao/songrepo')
//const mediaserver = require("mediaserver")
const _ = (require('lodash'))
const fs = require('fs')
const songdao = require('../dao/songrepo')
const songdir = require('../dao/songadrdao')
const songservice = require('../servicios/song')
const sessionsserv = require('../servicios/sessions')
//Prueba Generica
router.get("/",async function (req, res) {
    
    let query = req.query
    let range = req.headers.range
    const token = req.query.token
    const id = req.query.songid
    try {
        if (_.isEmpty(query)) {
        } else {
            await sessionsserv.estaLogeado(token)
            const stream = await songservice.getSongByUserId(  id, range, res)
            stream.pipe(res)
        }
    } catch (error) {
        res.sendStatus(error.sendStatus)
    }
})



module.exports = router