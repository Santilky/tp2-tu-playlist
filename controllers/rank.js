const express = require('express')
const router = express.Router()
const rankdao = require('../dao/valorarrepo')
const songdao = require('../dao/songrepo')
const userdao = require('../dao/userrepo')
const sessionsserv = require('../servicios/sessions')
const valoraserv = require('../servicios/valora')

router.post('/song/:token', async function (req, res) {
    const body = req.body
    const token = req.params.token
  //  console.log(body)
    try {
        const userid = await sessionsserv.estaLogeado(token)
     //   console.log(userid)
        const objN ={
            userId: userid,
            songId: parseInt(body.songId),
            score: parseInt(body.score)
        }
      //  console.log(objN)
        const obj = await valoraserv.publicarValSong(objN)
      //  console.log(obj)
        res.send(obj)    
    } catch (error) {
    //    console.log(error.message)
        res.sendStatus(error.sendStatus)
    }
})

router.get('/song/:token', async function (req, res) {
    const token = req.params.token
    try {
        if (token == null || token == undefined) {
            throw { sendStatus: 404, message: "ERROR DE PERMISOS" }
        }
        // const userid = await sessionsserv.estaLogeado(token)
        // const user = userdao.getUser(userid)
        // console.log('tengo un user!')
        // if (!user || user == undefined) {
        //     throw { sendStatus: 405, message: "ERROR EN EL PEDIDO" }
        // } 
        const objs = await valoraserv.getValSongs(token)
        if (objs == undefined || objs == null) {
            throw { sendStatus: 405, message: "ERROR EN EL PEDIDO"}
        }
      //  console.log(objs)
        res.send(objs)
    } catch (error) {
      //  console.log(error.message)
        res.sendStatus(error.sendStatus)
    }
})
router.post('/genre/:token', async function (req, res) {
    const body = req.body
    const token = req.params.token
    try {
        const userid =  await sessionsserv.estaLogeado(token)
        const objN = {
            genId: parseInt(body.genId),
            score: parseInt(body.score),
            userId: userid
        }    
     //   console.log('objB es ')
     //   console.log(objN)
        const obj = await valoraserv.publicarValGen(objN)
     //   console.log(obj)
      //  const obj = await rankdao.valorarGenero(objN)
        res.send(obj)
    } catch (error) {
        res.sendStatus(error.sendStatus)
    }
    
    // console.log(obj)
})
router.get('/genre/:token', async function (req, res) {
    const token = req.params.token
    
    if (!token || token == undefined || token == null) {
        res.send(await valoraserv.getGens())
    } else {
        try {
            const valgens = await valoraserv.getValGens(token)
           // console.log(valgens)
            res.send(valgens)
        } catch (error) {
        //    console.log(error.message)
            res.sendStatus(error.sendStatus)
        }
    }
})
router.get('/genre/', async function (req, res) {
    res.send(await songdao.getGeneros())
})

module.exports = router

