const express = require('express')
const router = express.Router()
//const db = require('../models/index')
const userdao = require('../dao/userrepo')
const negocio = require("../servicios/playlist").getPlaylist
const sessionsserv = require('../servicios/sessions')

router.get("/:token", async function (req, res) {
    const token = req.params.token
    
  
    try {
        if (token == null || token == undefined) {
            throw {sendStatus: 404, message: 'Sin Token'}
        }
        const userid = await sessionsserv.estaLogeado(token)
        if (userid == null || userid == undefined) {
            throw { sendStatus: 404, message: "Usuario no encontrado" }
        }
        const data = userdao.getUser(userid)
        if (data == undefined || data == null) {
            throw { sendStatus: 405, message: "ERROR EN EL PEDIDO" }
        }
          //  console.log('error antes del algo')
     //     console.log('entra a playlist')
            const playlist = await negocio(userid) 
          //  console.log('error post algo')         
            res.send(playlist)
       

    } catch (error) {
        // console.log(error.message)
        res.sendStatus(error.sendStatus)
    }
})
module.exports = router