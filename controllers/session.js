const express = require('express')
const router = express.Router()
//const db = require('../models/index')
const _ = (require('lodash'))
const sessionsserv = require('../servicios/sessions')

router.post("/", async function (req, res) {
  const query = req.query
  //console.log(query)
  try {
    if (_.isEmpty(query)) {
      throw {sendStatus: 404, message: "La query esta vacía"}
    }  
    let username = query.username
    let password = query.password
    let email = query.email
    if(password == undefined || username == undefined) {
      throw {sendStatus: 405, message: "La query tiene campos incompletos"}
    }
    let token = await  sessionsserv.login(username, email, password) 
    res.header({
        'Content-Type': 'application/json'
  });    res.send(token)

  } catch (error) {
    res.sendStatus(error.sendStatus)
  }
//  const user = userDao.login(body)
})

router.get("/:token",async function (req, res) {
  const token = req.params.token
  
  try {
    if (_.isEmpty(token)) {
      throw {sendStatus: 405, message: "ERROR EN EL PEDIDO"}
    }  
    
    let obj = { id :
      await sessionsserv.estaLogeado(token)}
    res.send(obj)

  } catch (error) {
   // console.log(error.message)
    res.sendStatus(error.sendStatus)
  }
//  const user = userDao.login(body)
})

module.exports = router