const rankdao = require('../dao/valorarrepo')
const songdao = require('../dao/songrepo') 
const userdao = require('../dao/userrepo')
const rankModel = require('../schemas/rank')
const rankGenModel = require('../schemas/rankgen')
const sessionsserv = require('../servicios/sessions')

async function publicarValGen(valgen) {
  const data = await rankGenModel.validate(valgen)
  const find = await rankdao.findRankGen(valgen) 
  let returned = null
  if (!find || find == undefined || find == null) {
    const gens = await songdao.getGeneros()
    const genFound = gens.find(r => r.id == valgen.genId)
   // console.log(genFound)
    if (!genFound || genFound == undefined || genFound == null) {
      throw { sendStatus: 404, message: "ERROR INEXISTENCIA" }
    }
    returned = await rankdao.addValidatedRankGen(valgen)
  } else {
 //   console.log(find)
 //   console.log(valgen)
    find.score = valgen.score
    await rankdao.updateValGen(find)
    returned = find
  }
 // console.log(returned)
  return returned
}

async function publicarValSong(valsong) {
//  console.log(valsong)
  const data = await rankModel.validate(valsong)
  const find = await rankdao.findRankSong(valsong) 
  let returned = null
 // console.log(find)
  if (!find || find == undefined || find == null) {
  //  console.log('entro')
    const songFound = await songdao.getSong(valsong.songId)
    if (!songFound || songFound == undefined || songFound == null) {
      throw { sendStatus: 404, message: "ERROR INEXISTENCIA" }
    }
  //  console.log(songFound)
  //  console.log('en teoria deberia andar')
   // valsong.genId = songFound.genre[0]
  //  console.log(valsong)
    returned = await rankdao.addValidatedRankSong(valsong)
  //  console.log('tiene que no romper')
  //  console.log(returned)
  } else {
    find.score = valsong.score
    await rankdao.updateValSong(find)
    returned = find
  }
  return returned
}

async function getValGens(token) {
 // console.log(token)
  const userid = await sessionsserv.estaLogeado(token)
  if (userid == null || userid == undefined) {
    throw { sendStatus: 405, message: "ERROR EN EL PEDIDO"}
  }
  const valgens = await rankdao.findRankGensById(userid)
 // console.log(valgens)
  return valgens
}
async function getValSongs(token) {
//  console.log(token)
  const userid = await sessionsserv.estaLogeado(token)
  if (userid == null || userid == undefined) {
    throw { sendStatus: 405, message: "ERROR EN EL PEDIDO"}
  }
  const valsongs = await rankdao.findRankSongsById(userid)
//  console.log(valsongs)
  return valsongs
}

async function getValGen(token, genId) {
  const userid = await sessionsserv.estaLogeado(token)
  if (userid == null || userid == undefined) {
    throw { sendStatus: 405, message: "ERROR EN EL PEDIDO"}
  }
} 

async function getGens() {
  return await songdao.getGeneros()
}


module.exports = {
  publicarValGen,
  publicarValSong,
  getValGens,
  getValGen,
  getGens,
  getValSongs
}