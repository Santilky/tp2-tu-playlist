//const usergen = require('./usergen')
//const filtros = require('./filtros')
//const canciones = require('/canciones')
// const usuarios = require('./fakedb/users').users
//const valgen = require('./fakedb/valgen').generos
//const valcanciones = require('./fakedb/valsong').valsong

//const generos = require('./fakedb/generos').generos
//const canciones = require('./fakedb/canciones').canciones
//const listasUsuarios = require('./fakedb/listasusuarios').lista
//const db = require('../db/db')
const filters = require("./filter").filterSongs
const songdao = require('../dao/songrepo')
const valdao = require('../dao/valorarrepo')

async function getPlaylist(userId) {
    //console.log("Entramos!! adntro")
    const valgens = await valdao.getRankGenerosUserId(userId)
//    console.log("===============================")
//    console.log(valgens.length)

   // let lista_can = [];
   // let listafinal = []
  // console.log(valgens)
  // console.log(valgens.length)
    let valCanciones = await valdao.getRankCancionesUser(userId)
    if (valgens == undefined || valgens == null || valgens.length < 1) {
    //    console.log('tiro error')
        throw error = {sendStatus: 402, message: "El usuario no tiene valoraciones por genero por lo cual no podemos brindarle musica todavia"}
    }
    const canciones = await songdao.getCanciones()
  //  console.log("===============================")
  //  console.log("canciones = "+ canciones.length)
  //  console.log("Hasta aca estamos bien")

    const listafinal = await filters(valCanciones, valgens, canciones)
    const playlist = agregarCancionesRandom(listafinal, 3)
 //   console.log(playlist)
    return playlist
}
function agregarCancionesRandom(lista_elegida, cant) {
    let playlist = []
    let i = 0;
    lista_candidata =   lista_elegida[0]

  //  console.log("Aca la hermosa lista" + (lista_candidata))
    while (i < cant && lista_elegida.length > 1) {
        let index = parseInt(Math.random() * (lista_elegida.length))
       
        playlist.push(lista_elegida.splice(index, 1)[0])
        i++;
    }
    return playlist;
}


module.exports = {
    getPlaylist,
    //filterSongs,
   // ordenarListaMejorVal
}
