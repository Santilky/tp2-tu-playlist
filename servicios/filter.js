const songdao = require('../dao/songrepo')

async function filterSongs( valSongs, valGens, canciones) {
//  console.log("1")

  let valoracionesXgenero = ordenarListaMejorVal(valGens)
//  console.log("2")
  let valoracionesXGenXCanciones = ordenarListaMejorVal(valSongs)
 // console.log("3")
  let valSongsGens = []
  let lista_can = []
  let listafinal = []
  let cant = 6
//  console.log("Hasta acaaaaa estamos bien")

  while (lista_can.length < 150 && valoracionesXgenero.length > 0) {
  //  console.log("loop!")

      let val = valoracionesXgenero.shift()
    //     console.log("===============================")
// console.log(val)
      // De cada genero agarra X cantidad de canciones disminuye a medida que el score es menor
      
      let elegidos = agregarCancionesRandom(canciones.filter(obj => obj.genId == val.genId), cant)
  //    console.log("Avanze!")

      lista_can = lista_can.concat(elegidos)
      valSongsGens = valSongsGens.concat(valoracionesXGenXCanciones.filter(valgs => valgs.genId == val.genId))
      if (cant > 2) {
          cant -= 2
      }
  }
  // console.log("lista_can " + lista_can.length)
  let i = 1
  for (const val of valSongsGens) {

      let indefinido
      indefinido = lista_can.find(can => can.id == val.songId)
      if (indefinido != undefined) {
          listafinal.push(indefinido)
          valSongsGens.shift()
      }
      i++
  }

  if (listafinal.length < 3) {
  //  console.log("Llego a sin valoraciones de tems")
      let valoracionesXgenerosOrdenados = ordenarListaMejorVal(valGens)
      let i = 5;
      for (const genero of valoracionesXgenerosOrdenados) {
          const cancionesGenero = await songdao.getCancionesPorGeneroID(genero.genId)
  //        console.log("Canciones x genero "+ genero.genId + ", en total:" + cancionesGenero.length)
          listafinal = listafinal.concat(agregarCancionesRandom(cancionesGenero, i ))
          if (i > 1) {
              i--
          }
      }
  }
  if (listafinal.length < 3) {
  //  console.log('Sigue sin valoraciones')
    const cans = await songdao.getCanciones()
    listafinal = listafinal.concat(agregarCancionesRandom(cans, 6))
  }
  return listafinal
}


function agregarCancionesRandom(lista_elegida,cant,playlist = []) {
  let i = 0;
 // console.log(playlist)

  while (i < cant && lista_elegida.length > 0) {
      let index = parseInt(Math.random() * (lista_elegida.length))
      playlist.push(lista_elegida.splice(index, 1)[0])
      i++;
  }
//  console.log("lista_elegida: " + lista_elegida.length)
//  console.log(playlist)
  return playlist;
}

function ordenarListaMejorVal(lista) {
  //Es una Lista pequeña
  const arr = [[], [], [], [], [], [], [], [], [], [], []] //array de 11 (0 a 10)
  for (const obj of lista) {
      arr[obj.score].push(obj)
  }
  const newList = [];
  for (let index = 0; index < 10; index++) {
      for (const obj of arr[10 - index]) {
          newList.push(obj)
      }
  }

  return newList
}

module.exports = {
  filterSongs
}