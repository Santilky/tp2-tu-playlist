const _ = (require('lodash'))
const fs = require('fs')
const songdao = require('../dao/songrepo')
const songdir = require('../dao/songadrdao')

async function getSongByUserId( id, range, res) {
  if (id == null || isNaN(parseInt(id)) || id == null) {
  //  console.log(id)
    throw { sendStatus: 404, message: "Empty Query" }
  }
  const dir = await songdir.getSongDirByID(id)
  //  console.log("dir es : " +dir)
  if (dir == undefined || dir == null || id == undefined || id == null) {
    throw { sendStatus: 404, message: "Empty Query" }
  } else {
    //var key = req.params.key;
    let stat
    // var music = 'music/' + key + '.mp3';
    try {
      stat = fs.statSync(dir.address)
    } catch (error) {
      throw { sendStatus: 404, message: "Internal error" }
    }
    let readStream;

    if (range !== undefined) {
      const parts = range.replace(/bytes=/, "").split("-")

      const partial_start = parts[0];
      const partial_end = parts[1];

      if ((isNaN(partial_start) && partial_start.length > 1) || (isNaN(partial_end) && partial_end.length > 1)) {
        throw { sendStatus: 500, message: "Sarasa"} //ERR_INCOMPLETE_CHUNKED_ENCODING
      }

      const start = parseInt(partial_start, 10);
      const end = partial_end ? parseInt(partial_end, 10) : stat.size - 1;
      const content_length = (end - start) + 1;

      res.status(206).header({
        'Content-Type': 'audio/mpeg',
        'Content-Length': content_length,
        'Content-Range': "bytes " + start + "-" + end + "/" + stat.size
      });

      readStream = fs.createReadStream(dir.address, { start: start, end: end });
    } else {
      res.header({
        'Content-Type': 'audio/mpeg',
        'Content-Length': stat.size
      });
      readStream = fs.createReadStream(dir.address)
    }
    return readStream
  }
}

module.exports = {getSongByUserId }