const userrepo = require('../dao/userrepo')
const userModel = require('../schemas/user')

async function registerUsuario(userObject) {

  const { error, value } = await userModel.validate(userObject)
  await userrepo.validarUsuarioRepetido(userObject.username, userObject.email)
  const userNew = await userrepo.agregarUsuarioValidado(userObject)
//  console.log("Usuario " + userObject.username + ". Añadido correctamente")
  return userNew
}

async function getUserById(id) {
  const user = await userrepo.getUser(id)
  if (!user || user == undefined) {
    throw { sendStatus: 404, message: "No se encontró usuario"}
} 
  return user
}

module.exports = {
  registerUsuario,
  getUserById
}