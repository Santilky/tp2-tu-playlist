const sessionsdao = require('../dao/sessiondao')
const usersdao = require('../dao/userrepo')
const tokenschema = require('../schemas/token')
const loginobj = require('../schemas/loginobj')
const _ = require('lodash')
function randomString(length) {
  const chars = '0123456789abcdefghijklmnopqrstuvwxyz'
  let result = ''
  for (let i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)]
  return result
}


function generarToken () {
  return randomString(25)
}

function login(username, email, password) {
  const usero = {username: username, email: email, password: password}
  const {err, value} =  loginobj.validate(usero)

  const id = usersdao.buscarUserMailOPass(usero)
  const session = {
    token: generarToken(),
    id: id
  }
  sessionsdao.guardarSession(session)
  return {token: session.token}
}

async function estaLogeado(token) {
//  console.log(token)
  if (token == null || token == undefined){
    throw {sendStatus: 505, message: "Error en el modelo  de usuario"} 
  }
  
   let userInv = sessionsdao.getIdByToken(token)
  // console.log(userInv)
   if(userInv == null || userInv == undefined){
    throw {sendStatus: 402, message: "Error en el modelo  de usuario"} 
   }
  
 //const {err, value} = await tokenschema.validate(token)
  return userInv
}

module.exports = {login, estaLogeado}