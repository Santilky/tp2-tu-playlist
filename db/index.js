const Sequelize = require('sequelize')
const generos = require('./generos')
const songs = require('./songs')
const songsadr = require('./songdir')
const users = require('./user')
const valgen = require('./valgen')
const valsongs = require('./valsongs')

let db = {}

const credenciales = {
  user: 'root',
  password: '1qaz2wsx',
  host: 'localhost',
  database: 'tuplaylist',
}

async function crearDB() {
const sequelize = new Sequelize(credenciales.database, credenciales.user, credenciales.password, {
  host: credenciales.host,
  dialect: 'mysql',
  logging: false,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
})
try {
  await sequelize.authenticate()
  db.generos = generos(sequelize, Sequelize)
  db.songs = songs(sequelize, Sequelize)
  db.songsadr = songsadr(sequelize, Sequelize)
  db.users = users(sequelize, Sequelize)
  db.valgen = valgen(sequelize, Sequelize)
  db.valsongs = valsongs(sequelize, Sequelize)
  await sequelize.sync({ force: false })
} catch (error) {
  throw 'No se ha podido conectar o sincronizar con MySQL'
}
}

module.exports = {db, crearDB}