module.exports = (sequelize, Sequelize) => {
  let User = sequelize.define('user', {
      username: Sequelize.STRING,
      password: Sequelize.STRING,
      email: Sequelize.STRING
  })
  return User
}