module.exports = (sequelize, Sequelize) => {
  let ValGen = sequelize.define('valgen', {
      genId: Sequelize.INTEGER,
      score: Sequelize.INTEGER,
      userId: Sequelize.INTEGER
  })
  return ValGen
}