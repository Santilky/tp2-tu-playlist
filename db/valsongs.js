module.exports = (sequelize, Sequelize) => {
  let ValSongs = sequelize.define('valsongs', {
      songId: Sequelize.INTEGER,
      score: Sequelize.INTEGER,
      userId: Sequelize.INTEGER
  })
  return ValSongs
}