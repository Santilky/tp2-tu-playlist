module.exports = (sequelize, Sequelize) => {
  let Songs = sequelize.define('songs', {
    artist: Sequelize.STRING,
    album: Sequelize.STRING,
    title: Sequelize.STRING,
    genre : Sequelize.INTEGER
  })
  return Songs
}