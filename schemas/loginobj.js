const Joi = require('@hapi/joi');

const user = Joi.object({
    username: Joi.string()
        .alphanum()
        .min(6)
        .max(20)
        .required(),

    password: Joi.string()
        .pattern(/^[a-zA-Z0-9]{3,30}$/),

  //  access_token: [
  //      Joi.string(),
  //      Joi.number()
  //  ],

  //  birth_year: Joi.number()
  //      .integer()
  //      .min(1900)
  //      .max(2013),

    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
})

async function validate (login) {
    // Este metodo tira error
    try {
        return await user.validateAsync(login);    
    } catch (error) {
        throw {sendStatus: 406, message: "Error en el modelo login"} 
    }
    
}

module.exports = {
     validate
}