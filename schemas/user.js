const Joi = require('@hapi/joi');

const user = Joi.object({
    username: Joi.string()
        .alphanum()
        .min(6)
        .max(20)
        .required(),

    password: Joi.string()
        .pattern(/^[a-zA-Z0-9]{3,30}$/),

    repeat_password: Joi.ref('password'),

  //  access_token: [
  //      Joi.string(),
  //      Joi.number()
  //  ],

  //  birth_year: Joi.number()
  //      .integer()
  //      .min(1900)
  //      .max(2013),

    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
})
    .with('username', 'email')
  //  .xor('password', 'access_token')
    .with('password', 'repeat_password');

async function validate (userNuevo) {
    // Este metodo tira error
    try {
        return await user.validateAsync(userNuevo);    
    } catch (error) {
        throw {sendStatus: 406, message: "ERROR EN LA VALIDACION DE DATOS"} 
    }
    
}

module.exports = {
    user, validate
}