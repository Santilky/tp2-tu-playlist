const Joi = require('@hapi/joi');

const valoracion = Joi.object({
    userId: Joi.number().integer().min(1)
        .required(),
    genId: Joi.number().integer().min(1)
    .required(),
    score: Joi.number().integer().min(0).max(10).required()
})
  //  .with('username', 'email')
  //  .xor('password', 'access_token')
  //  .with('password', 'repeat_password');

async function validate (rankNuevo) {
    // Este metodo tira error
    //return await valoracion.validateAsync(rankNuevo);
  //  console.log("Validemos!")
  //  console.log(rankNuevo)
    try {
        return await valoracion.validateAsync(rankNuevo);    
    } catch (error) {
        throw {sendStatus: 406, message: "ERROR EN LA VALIDACION DE DATOS "} 
    }
}

module.exports = {
 validate
}