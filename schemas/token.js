const Joi = require('@hapi/joi');

const token = Joi.object({
   

    token: Joi.string()
        .pattern(/^[a-z0-9]{3,30}$/).required()
  //  access_token: [
  //      Joi.string(),
  //      Joi.number()
  //  ],

  //  birth_year: Joi.number()
  //      .integer()
  //      .min(1900)
  //      .max(2013),
})


async function validate (tokenNuevo) {
    // Este metodo tira error
    try {
        return await token.validateAsync(tokenNuevo);    
    } catch (error) {
        throw {sendStatus: 406, message: "ERROR EN LA VALIDACION DE DATOS"} 
    }
    
}

module.exports = {
     validate
}