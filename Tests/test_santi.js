const request = require('request-promise-native')
const util = require('util')

const tests = [crearUsuarioValido,
  logearUsuarioValido,
  getUsuarioValido,
  getPlayListSinValgen,
  getGenerosVacio,
  getGenerosYGuardarlos,
  getValSongsVacias,
  cargarValGens,
  getPlayListConValGens,
  cargarValSongs,
  getSongValida,
  crearUsuariosInvalidos,
  crearValGensInvalidas,
  crearValsongsInvalidas,
  getSongInvalida,
  rompeTokens


]
const errores = []
let token = null
let testIndex = 0
let aprobados = 0
let generos = []

const credenciales = {
  username: randomString(6),
  password: '123456',
  repeat_password: '123456',
  email: randomString(10) + '@' + randomString(8) + '.com'
}

function randomString(length) {
  const chars = '0123456789abcdefghijklmnopqrstuvwxyz'
  let result = ''
  for (let i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)]
  return result
}

async function postUser(body) {
  let post = {
    method: 'POST',
    uri: "http://localhost:8000/api/users/",
    json: true,
    body: body
  }
  return await request(post)
}

async function userLogin(user, pass) {
  let login = {
    method: 'POST',
    uri: "http://localhost:8000/api/session?username=" + user + "&password=" + pass
  }
  return await request(login)
}

async function getUserId() {
  let post = {
    method: 'GET',
    uri: "http://localhost:8000/api/session/" + token
  }
  return await request(post)
}

async function getGeneros() {
  let post = {
    method: 'GET',
    uri: "http://localhost:8000/api/rank/genre/"
  }
  return await request(post)
}

async function valorarGenero(object) {
  let post = {
    method: 'POST',
    uri: "http://localhost:8000/api/rank/genre/" + token,
    json: true,
    body: object
  }
  return await request(post)
}
async function obtenerGenerosValorados() {
  let result = false
  let post = {
    method: 'GET',
    uri: "http://localhost:8000/api/rank/genre/" + token
  }
  return await request(post)
}

async function valorarCancion(valsong) {
  let post = {
    method: 'POST',
    uri: "http://localhost:8000/api/rank/song/" + token,
    json: true,
    body: valsong
  }
  return await request(post)
}

async function obtenerCancionesValoradas() {
  let post = {
    method: 'GET',
    uri: "http://localhost:8000/api/rank/song/" + token
  }
  return await request(post)
}
async function obtenerPlaylist() {
  let post = {
    method: 'GET',
    uri: "http://localhost:8000/api/playlist/" + token
  }
  return await request(post)
}

async function pedirUnTema( tema) {
  let get = {
    method: 'GET',
    uri: 'http://localhost:8000/api/songs/?songid=' + tema + '&token=' + token
  }
 // console.log(get.uri)
  return await request(get)
}

async function crearUsuarioValido() {
  try {
    testIndex++
    const req = await postUser(credenciales)
    aprobados++
  } catch (error) {
    errores.push('POST user, error: ' + error.statusCode)
  }
}
async function logearUsuarioValido() {
  try {
    testIndex++
    const resp = await userLogin(credenciales.username, credenciales.password)
    const tokenresp = await JSON.parse(resp)
    token = tokenresp.token
    // console.log(token)
    aprobados++
  } catch (error) {
    errores.push('User login, error: ' + error.statusCode)
  }
}
async function getUsuarioValido() {
  try {
    testIndex++
    const resp = await getUserId(token)
    aprobados++
  } catch (error) {
    errores.push('GET User, error: ' + error.statusCode)
  }
}

async function getPlayListSinValgen() {
  try {
    testIndex++
    const playlist = await obtenerPlaylist()
    errores.push('GET Playlist, se esperaba 402, se obtuvo un resultado')
  } catch (error) {
    if (error.statusCode == 402) {
      aprobados++
    } else {
      errores.push('GET Playlist, se esperaba 402, se obtuvo ' + error.statusCode)
    }
  }
}

async function getGenerosVacio() {
  try {
    testIndex++
    const result = await obtenerGenerosValorados()
    const parsed = await JSON.parse(result)
    if (parsed.length > 0) {
      errores.push('GET ValGens, deberia ser vacio, contiene informacion')
    } else {
      aprobados++
    }
  } catch (error) {
    errores.push('GET ValGens, devolvió error ' + error.statusCode)
  }
}

async function getGenerosYGuardarlos() {
  try {
    testIndex++
    const gens = await getGeneros()
    const parsed = await JSON.parse(gens)
    if (parsed.length > 0) {
      aprobados++
      generos = parsed
    } else {
      errores.push('GET Generos, sin información')
    }
  } catch (error) {
    errores.push('GET Generos se obtuvo error ' + error.statusCode)
  }
}

async function getValSongsVacias() {
  try {
    testIndex++
    const result = await obtenerCancionesValoradas()
    const parsed = await JSON.parse(result)
    if (parsed.length > 0) {
      errores.push('GET ValSongs deberia ser vacio')
    } else {
      aprobados++
    }
  } catch (error) {
    errores.push('GET ValSongs recibió error ' + error.statusCode)
  }
}

async function cargarValGens() {
  const objs = [{
    genId: 2,
    score: 10
  },
  {
    genId: 1,
    score: 8
  },
  {
    genId: 3,
    score: 6
  },
  {
    genId: 8,
    score: 6
  },
  {
    genId: 4,
    score: 8
  },
  {
    genId: 5,
    score: 7
  }
  ]
  testIndex++
  let errorAlCargar = 0
  for (const obj of objs) {
    try {
      const result = await valorarGenero(obj)
    } catch (error) {
      errorAlCargar++
    }
  }
  testIndex++
  if (errorAlCargar == 0) {
    aprobados++
    try {
      const result = await obtenerGenerosValorados()
      const parsed = await JSON.parse(result)
    //  console.log(parsed)
      if (parsed.length == objs.length) {
        aprobados++
      } else {
        errores.push('VALGENS Se esperaban ' + objs.length + ' y se obtuvieron ' + parsed.length)
      }
    } catch (error) {
      errores.push('GET ValGens recibió error' + error.statusCode)
    }
  } else {
    errores.push('POST VALGEN Algun objeto dio error')
  }
}

async function getPlayListConValGens() {
  try {
    testIndex++
    const playlist = await obtenerPlaylist()
    const parsed = await JSON.parse(playlist)
    if (parsed.length == 3) {
      aprobados++
    } else {
      errores.push('GET PlayList la Lista deberia cntener 3 canciones, tiene ' + parsed.length)
    }
  } catch (error) {
    errores.push('GET PlayList recibio error ' + error.statusCode)
  }
}

async function cargarValSongs() {
  const objs = [{
    songId: 2,
    score: 10
  },
  {
    songId: 1,
    score: 8
  },
  {
    songId: 3,
    score: 6
  },
  {
    songId: 8,
    score: 6
  },
  {
    songId: 4,
    score: 8
  },
  {
    songId: 5,
    score: 7
  }
  ]
  testIndex++
  testIndex++

  let erroresTemp = 0
  for (const obj of objs) {
    try {
      const result = await valorarCancion(obj)
    } catch (error) {
      erroresTemp++
    }
  }
  if (erroresTemp == 0) {
    aprobados++
    try {
      const result = await obtenerCancionesValoradas()
      const parsed = await JSON.parse(result)
      if (parsed.length == objs.length) {
        aprobados++
      } else {
        errores.push('GET ValSongs se esperaban ' + objs.length + ' resultados, se obtuvieron: ' + parsed.length)
      }
    } catch (error) {
      errores.push('GET ValSongs se obtuvo un error ' + error.statusCode)
    }
  } else {
    errores.push('POST ValSongs: No se efectuaron bien todos los tests')
  }
}

async function getSongValida() {
  try {
    testIndex++
    const ids = [2, 5, 7, 8]
    for (const id of ids) {
      await pedirUnTema(id)
    }
    aprobados++
  } catch (error) {
    errores.push('GET Song error ' + error.statusCode)
  }
}

async function crearUsuariosInvalidos() {
  const usuarios = [{
    password: '123456',
    repeat_password: '123456',
    email: 'test@test.com'
  }, {
    username: randomString(6),
    password: '123456',
    email: randomString(10) + '@' + randomString(8) + '.com'
  }, {
    username: randomString(6),
    password: '123456',
    email: randomString(10) + '@' + randomString(8) + '.com'
  }, {
    username: randomString(6),
    password: '123456',
    repeat_password: '12345',
    email: randomString(10) + '@' + randomString(8) + '.com'
  }, {
    username: randomString(6),
    password: '123456',
    repeat_password: '123456',
    email: 'asdsad'
  }
  ]
  testIndex++

  try {
    for (const userinv of usuarios) {
      try {
        await postUser(userinv)
        throw {statusCode: 1000}
      } catch (error) {
        if (error.statusCode == 406) {

        } else {
          throw {statusCode: error.statusCode}
        }
      }
    }
    aprobados ++
  } catch (error) {
    if (error.statusCode != 1000) {
      errores.push('POST User Se obtuvo un error inesperado ' + error.statusCode)
    } else {
      errores.push('POST User Un objeto invalido fue validado correctamente')
    }
  }
}

async function crearValGensInvalidas() {
  const objs = [{
    genId: 15,
  },{
    score: 13
  },
  {
    genId: 10,
    score: -10
  },{
    genId: 'hamburgonza',
    score: 8
  },{
    genId: 2,
    score: 'hambugonza'
  }, {

  }
  ]
  const objetoGenNoExiste = {
    genId: 200,
    score: 3
  }
  testIndex++
  testIndex++
  try {
    for (const obj of objs) {
      try {
        const result = await valorarGenero(obj)
        throw {statusCode: 1000}
      } catch (error) {
        if (error.statusCode == 406) {
  
        } else {
          throw error.statusCode
        }
      }
    }
    aprobados++
  } catch (error) {
    if (error.statusCode == 1000) {
      errores.push('POST ValGen invalido fue aceptado')
    } else {
      errores.push('Post ValGens se esperaba error 406 y se obtuvo' + error.statusCode)
    }
  }
  try {
    const result = await valorarGenero(objetoGenNoExiste)
    errores.push('POST ValGen se creo un objeto invalido' )
  } catch (error) {
    if (error.statusCode == 404) {
      aprobados++
    } else {
      errores.push('POST ValGen Se esperaba 404, se obtuvo '+ error.statusCode)
    }
  }
}

async function crearValsongsInvalidas() {
  const objs = [{
    songId: 15,
  },{
    score: 13
  },
  {
    songId: 10,
    score: -10
  },{
    songId: 'hamburgonza',
    score: 8
  },{
    songId: 2,
    score: 'hambugonza'
  }, {

  }
  ]
  const objetoGenNoExiste = {
    songId: 200,
    score: 3
  }
  testIndex++
  testIndex++
  try {
    for (const obj of objs) {
      try {
        const result = await valorarCancion(obj)
        throw {statusCode: 1000}
      } catch (error) {
        if (error.statusCode == 406) {
  
        } else {
          throw error.statusCode
        }
      }
    }
    aprobados++
  } catch (error) {
    if (error.statusCode == 1000) {
      errores.push('POST ValSongs invalido fue aceptado')
    } else {
      errores.push('Post ValSongs se esperaba error 406 y se obtuvo' + error.statusCode)
    }
  }
  try {
    const result = await valorarCancion(objetoGenNoExiste)
    errores.push('POST ValSong se creo un objeto invalido' )
  } catch (error) {
    if (error.statusCode == 404) {
      aprobados++
    } else {
      errores.push('POST ValSong Se esperaba 404, se obtuvo '+ error.statusCode)
    }
  }
}

async function getSongInvalida() {
  try {
    testIndex++
    const ids = [59, 'salchicha', {id:56}, -150]
    for (const id of ids) {
      try {
        await pedirUnTema(id)
        throw {statusCode: 1000}  
      } catch (error) {
        if (error.statusCode == 404) {
          
        } else {
          throw {statusCode: error.statusCode}
        }
      }
    }
    aprobados++
  } catch (error) {
    if (error.statusCode == 1000) {
      errores.push('GET Song Se realizo un pedido con id invalido')
    } else {
      errores.push('GET Song se esperaba error 404, se obtuvo ' + error.statusCode)
    } 
  }
}

async function rompeTokens() {
  testIndex++
  token = '23434'
  try {
    try {
      await getUserId()  
      throw {statusCode: 1000}  
    } catch (error) {
      if (error.statusCode != 401) {
        throw {statusCode: error.statusCode}
      }
    }  
    try {
      await valorarGenero({genId: 4, score: 8})  
      throw {statusCode: 1000}  
    } catch (error) {
      if (error.statusCode != 401) {
        throw {statusCode: error.statusCode}
      }
    }  
    try {
      await valorarCancion({songId: 4, score: 8})  
      throw {statusCode: 1000}  
    } catch (error) {
      if (error.statusCode != 401) {
        throw {statusCode: error.statusCode}
      }
    }  
    try {
      await obtenerGenerosValorados()  
      throw {statusCode: 1000}  
    } catch (error) {
      if (error.statusCode != 401) {
        throw {statusCode: error.statusCode}
      }
    }
    try {
      await obtenerCancionesValoradas()  
      throw {statusCode: 1000}  
    } catch (error) {
      if (error.statusCode != 401) {
        throw {statusCode: error.statusCode}
      }
    } 
    try {
      await obtenerPlaylist()  
      throw {statusCode: 1000}  
    } catch (error) {
      if (error.statusCode != 401) {
        throw {statusCode: error.statusCode}
      }
    }
    try {
      await pedirUnTema(2)  
      throw {statusCode: 1000}  
    } catch (error) {
      if (error.statusCode != 401) {
        throw {statusCode: error.statusCode}
      }
    }
    aprobados++
  } catch (error) {
    if (error.statusCode == 1000) {
      errores.push('Algun Test paso el test de validacion con token incorrecto')
    } else {
      errores.push('Validacion Se obtuvo un Status Code incorrecto: ' + error.statusCode)
    }
  }
  
  
  
}
async function main() {
  for (const test of tests) {
    await test()
  }
  console.log('Se Realizaron exitosamente ' + aprobados + '/' + testIndex)
  for (const error of errores) {
    console.log(error)
  }
}
main()