const request = require('request-promise-native')
const util = require('util')

async function crearUsuario() {
  let result = false
  let post = {
    method: 'POST',
    uri: "http://localhost:8000/api/users/",
    json: true,
    body: {
      username: "Juanse",
      password: "12345",
      repeat_password: "12345",
      email: "Janito@123.com"
    }
  }

  try {
    const response = await request(post)
    console.log("crear usuario: ok")
    result = true
  } catch (error) {
    if (error.statusCode === 401) {
      console.log("crear usuario: error - usuario ya existe")
    } else if (error.statusCode === 406) {
      console.log("crear usuario: datos malformados en el body")
    }
    else {
      console.log("crear usuario: error del lado del back")
    }
  }
  return result
}
  
async function loguearUsuario() {
  let result = false
  let post = {
    method: 'POST',
    uri: "http://localhost:8000/api/session?username=Juanse&password=12345"
  }

  try {
    let response = await request(post)
    response = await JSON.parse(response) 
    if (response != null && response != undefined) {
      tokenForTests = response.token
      console.log("loguear usuario y obtener sesión: ok")
      result = true
    } else {
      console.log("loguear usuario y obtener sesión: error en el logueo")
    }
  } catch (error) {
    console.log("loguear usuario y obtener sesión: error obteniendo el token")
  }
  return result;
}

async function recuperarIdUsuario(token) {
  let result = false
  let post = {
    method: 'GET',
    uri: "http://localhost:8000/api/session/" + token
  }

  try {
    let response = await request(post)
    response =  await JSON.parse(response)
    if(response.id === 3){
      console.log("recuperar id usuario vía token: ok")
      result = true
    } else {
      console.log("recuperar id usuario vía token: error - no devolvió el id buscado")
    }
  } catch (error) {
    console.log("recuperar id usuario vía token: error en el token de acceso")
  } 
  return result
}

async function recuperarUsuario(token) {
  let result = false
  let post = {
    method: 'GET',
    uri: "http://localhost:8000/api/users/" + token,
  }

  try {
    let response = await request(post)
    response = await JSON.parse(response)
    if(response.user.username =='Juanse'  && response.user.id == 3){
      console.log("recuperar usuario vía token: ok")
      result = true
    } else {
      console.log("recuperar usuario vía token: error - no devolvió el usuario buscado")
    }
  } catch (error) {
    console.log("recuperar usuario vía token: error en el token de acceso")
  } 
  return result
}
  
async function obtenerGenero() {
  let result = false
  let genero_elegido = {
    name : 'INSTRUMENTAL',
    id : 3
  }
  let post = {
    method: 'GET',
    uri: "http://localhost:8000/api/rank/genre/"
  }

  try {
    let response = await request(post)
    response =  await JSON.parse(response)
    if (response[2].name === genero_elegido.name && response[2].id == genero_elegido.id) {
      console.log("obtener género: ok")
      result = true
    } else {
      console.log("obtener género: error - no devolvió el género buscado")
    }
  } catch (error) {
    console.log("obtener género: error al obtener género")
  }
  return result
}
  
async function subirUnGenero(token ) {
  let result = false
  let post = {
    method: 'POST',
    uri: "http://localhost:8000/api/rank/genre/" + token,
    json: true,
    body: {
      genId: 3,
      score: 10
    }
  }
  
  try {
    const response = await request(post)
    console.log("valorar un género: ok")
    result = true
  } catch (error) {
    if (error.statusCode == 406) {
      console.log("valorar un género: error en los datos del body")
    } else {
      console.log("valorar un género: error en el token de acceso")
    }
  }
  return result
}

async function obtenerGeneroValorado(token){
  let result = false
  let post = {
    method: 'GET',
    uri: "http://localhost:8000/api/rank/genre/" + token
  }
  
  try {
    let response = await request(post)
    response =  await JSON.parse(response)
    if (response[0].genId == 3 && response[0].score == 10  && response[0].userId == 3) {
      console.log("obtener valoración de género: ok")
      result = true
    } else {
      console.log("obtener valoración de género: error - no es el género pedido")
    }
  } catch (error) {
    console.log("obtener valoración de género: error en el token de acceso")
  } 
  return result
}
  
async function valorarUnaCancion(token) {
  let result = false
  let post = {
    method: 'POST',
    uri: "http://localhost:8000/api/rank/song/" + token,
    json: true,
    body: {
      genId: 3,
      score: 10,
      songId:3,
    }
  }
  
  try {
    const response = await request(post)
    console.log("valorar una canción: ok")
    result = true
  } catch (error) {
  if(error.statusCode === 406){
    console.log("valorar una canción: error en el body")
  }
  console.log("valorar una canción: error en el token de acceso")
  } 
  return result
}

async function obtenerCancion(token) {
  let result = false
  let post = {
    method: 'GET',
    uri: "http://localhost:8000/api/rank/song/" + token
  }

  try {
    let response = await request(post)
    let gen  = await JSON.parse(response)
    if (gen[0].genId === 3 && gen[0].songId === 3 && gen[0].score === 10) {
      console.log("obtener valoración de canción: ok")
      result = true
    } else {
      console.log("obtener valoración de canción: error - no es la canción pedida")
    }
  } catch (error) {
    console.log("obtener valoración de canción: error en el token de acceso")
  } 
  return result
}

async function obtenerPlaylist(token) {
  let result = false
  let post = {
    method: 'GET',
    uri: "http://localhost:8000/api/playlist/" + token
  }

  try {
    let response = await request(post)
    response = await JSON.parse( response )
    if(response != null || response != undefined) {
      console.log("obtener playlist: ok")
      result = true
    } else {
      console.log("obtener playlist: error en la obtención de la playlist")
    }
  } catch (error) {
    console.log("obtener playlist: error en el token de acceso")
  } 
  return result
}

async function pedirUnTema(token) {
  let result = false
  let get = {
    method: 'GET',
    uri: 'http://localhost:8000/api/songs/?songid=8&token=' + token
  }
  try {
    const response = await request(get)
    if (response) {
      console.log("obtener y reproducir una canción: ok")
      result = true
    } else {
      console.log("obtener y reproducir una canción: error en la obtención de la canción")
    }
  } catch (error) {
    console.log("obtener y reproducir una canción: error en el token de acceso")
  }
  return result
}

async function testCrearUsuarioConBodyInvalido() {
  let result = false
  let post = {
    method: 'POST',
    uri: "http://localhost:8000/api/users/",
    json: true,
    body: {
      password: "12345",
      repeat_password: "12345",
      email: "hola@123.com"
    }
  }

  try {
    const response = await request(post)
    console.log("crear usuario con body inválido: error en el servidor - debería haber devuelto un error")
  } catch (error) {
    if (error.statusCode === 406) {
      console.log("crear usuario con body inválido (espero un error): ok")
      result = true
    } else {
      console.log("crear usuario con body inválido: error en el servidor")
    }
  }
  return result
}
  
async function obtenerGeneroValoradoConTokenInvalido(token) {
  let result = false
  let post = {
    method: 'GET',
    uri: "http://localhost:8000/api/rank/genre/" + token+"TEST"
  }

  try {
    let response = await request(post)
    console.log("obtener valoración de género con token inválido: error en el servidor - debería haber devuelto un error")
  } catch (error) {
    if(error.statusCode === 401){
      console.log("obtener valoración de género con token inválido (espero un error): ok")
      result = true
    } else {
      console.log("obtener valoración de género con token inválido: error en el servidor")
    }
  }
  return result
}

async function subirGeneroConBodyVacio(token) {
  let result = false
  let post = {
    method: 'POST',
    uri: "http://localhost:8000/api/rank/genre/" + token,
    json: true,
    body: {

    }
  }

  try {
    const response = await request(post)
    console.log("valorar un género con body vacío: error en el servidor")
  } catch (error) {
    if (error.statusCode === 406) {
      console.log("valorar un género con body vacío (espero un error): ok")
      result = true
    } else {
      console.log("valorar un género con body vacío: error en el servidor")
    }
  }
  return result
}

async function valorarUnaCancionBodyInvalido(token) {
  let result = false
  let post = {
    method: 'POST',
    uri: "http://localhost:8000/api/rank/song/" + token,
    json: true,
    body: {
      genId: 300000000000000000000,
      score: 10
    }
  }

  try {
    const response = await request(post)
    console.log("valorar una canción con body inválido: error en el servidor")
  } catch (error) {
    if (error.statusCode === 406) {
      console.log("valorar una canción con body inválido (espero un error): ok")
      result = true
    }
    else {
      console.log("valorar una canción con body inválido: error en el servidor")
    }
  }
  return result
}

async function loguearUsuarioSinBody() {
  let result = false
  let post = {
      method: 'POST',
      uri: "http://localhost:8000/api/session"
  }

  try {
    const response = await request(post)
    console.log("loguear un usuario sin body: error en el servidor")
  } catch (error) {
    if (error.statusCode === 404) {
      console.log("loguear un usuario sin body (espero un error): ok")
      result = true
    }
    else {
      console.log("loguear un usuario sin body: error en el servidor")
    }
  }
  return result
}

async function logearUserConBodyInvalido() {
  let result = false
  let post = {
    method: 'POST',
    uri: "http://localhost:8000/api/users/",
    json: true,
    body: {
      errorTest: "Juanse",
      errorTest2: "12345",
      errorTest3: "12345",
      errorTest4: "Janito@123.com"
    }
  }

  try {
    const response = await request(post)
    console.log("loguear un usuario con body inválido : error en el servidor")
  } catch (error) {
    if (error.statusCode === 406) {
      console.log("loguear un usuario con body inválido (espero un error): ok")
      result = true
    }
    else {
      console.log("loguear un usuario con body inválido : error en el servidor")
    }
  }
  return result
}

var tokenForTests = null

async function main() {
  let exitos = 0

  console.log("Tests TuPlaylist:")

  const tests = [
    crearUsuario,
    loguearUsuario,
    recuperarIdUsuario,
    recuperarUsuario,
    obtenerGenero,
    subirUnGenero,
    obtenerGeneroValorado,
    valorarUnaCancion,
    obtenerCancion,
    obtenerPlaylist,
    pedirUnTema,
    testCrearUsuarioConBodyInvalido,
    obtenerGeneroValoradoConTokenInvalido,
    subirGeneroConBodyVacio,
    valorarUnaCancionBodyInvalido,
    loguearUsuarioSinBody,
    logearUserConBodyInvalido
  ]

  for (const test of tests) {
      exitos += (await test(tokenForTests)) ? 1 : 0
  }

  console.log(`\nresultado de las pruebas: ${exitos}/${tests.length}`)
}
setTimeout(main, 2000)